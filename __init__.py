# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import crop
from . import location
from . import quality
from . import purchase
from . import sale
from . import product
from . import production
from . import stock
from . import party
from . import charge
from . import invoice
from . import exportation
from . import work
from . import genetics
from . import move
from . import account
from . import supplier_certificate


def register():
    Pool.register(
        configuration.Configuration,
        sale.AduanaDetailedStart,
        account.PrintSupplierCertificateStart,
        account.InvoiceAuthorization,
        crop.Kind,
        crop.Activity,
        crop.Variety,
        crop.VarietyStage,
        crop.VarietyStageActivity,
        crop.VarietyStageActivitySupply,
        crop.FarmingActivityShipmentInternal,
        crop.FarmingStage,
        crop.Crop,
        crop.CropStage,
        crop.CropStageActivity,
        crop.CropStageActivitySupply,
        crop.FarmingCropLot,
        crop.FarmingCropLotBed,
        crop.CropForecastStart,
        crop.CropForecastWeekStart,
        crop.CropActivitiesStart,
        crop.CropSuppliesStart,
        crop.SupplyMoves,
        crop.CropStageLoss,
        location.FarmingLocationLotBed,
        location.FarmingLocationLot,
        location.FarmingLocation,
        move.Move,
        genetics.FarmingSeed,
        quality.QualityTest,
        quality.QualityAnalysis,
        quality.ProductSpecies,
        quality.ICACertificate,
        quality.Phytosanitary,
        quality.ExportationPhyto,
        quality.ExportationPhytoLine,
        quality.PhytoStock,
        quality.PhytoStockMove,
        stock.StockMove,
        stock.PythoMove,
        stock.ShipmentIn,
        stock.StockLot,
        purchase.Purchase,
        purchase.PurchaseLine,
        purchase.PurchaseFarmingStart,
        purchase.GroupingPurchasesStart,
        purchase.UpdatePurchaseLineStart,
        sale.Sale,
        sale.SaleLine,
        sale.SaleLineKit,
        sale.SaleLineKitComponent,
        sale.GroupingSalesStart,
        sale.PortfolioDetailedStart,
        supplier_certificate.SupplierCertificate,
        supplier_certificate.SupplierCetificateInvoiceLine,
        supplier_certificate.SupplierCetificateParty,
        product.Template,
        product.ProductStyle,
        product.HarmonizedTariffSchedule,
        production.Production,
        production.ProductionSummaryStart,
        production.MaterialsForecastStart,
        production.Operation,
        production.ProductionTask,
        party.Party,
        party.Company,
        party.CustomsGlobal,
        charge.ExportationCharge,
        charge.ExportationPort,
        charge.ExportationChargeLine,
        charge.ChargeVehicle,
        charge.DispatchControlStart,
        invoice.Invoice,
        invoice.InvoiceLine,
        quality.PhytoMovesStart,
        quality.PurchaseMonitoringStart,
        exportation.DEX,
        exportation.DEXLine,
        exportation.ForeignExhangeTrading,
        exportation.ForeignExhangeTradingLine,
        exportation.SelectLinesAsk,
        work.Work,
        module='farming', type_='model')
    Pool.register(
        charge.DispatchControl,
        purchase.PurchaseFarming,
        purchase.GroupingPurchases,
        purchase.UpdatePurchaseLine,
        production.ProductionSummary,
        production.MaterialsForecast,
        production.ProductionForceDraft,
        production.DuplicateTask,
        sale.SaleForceDraft,
        sale.ReturnSale,
        sale.GroupingSales,
        sale.SaleChangeProcessing,
        sale.PortfolioDetailed,
        sale.AduanaDetailed,
        quality.PhytoMoves,
        quality.PurchaseMonitoring,
        stock.ShipmentOutProcess,
        exportation.SelectLines,
        crop.CropForecast,
        crop.CropForecastWeekWizard,
        crop.CropActivitiesWizard,
        crop.CropSuppliesWizard,
        account.PrintSupplierCertificate,
        module='farming', type_='wizard')
    Pool.register(
        purchase.PurchaseFarmingReport,
        production.ProductionSummaryReport,
        production.MaterialsForecastReport,
        charge.ExportationChargeReport,
        charge.LetterOfResponsability,
        charge.DispatchControlReport,
        quality.ExportationPhytosanitaryReport,
        quality.PhytoMovesReport,
        quality.PurchaseMonitoringReport,
        sale.PortfolioDetailedReport,
        sale.AduanaDetailedReport,
        exportation.ForeignExhangeTradingReport,
        crop.CropForecastReport,
        crop.CropActivitiesReport,
        crop.CropSuppliesReport,
        crop.CropForecastWeekReport,
        crop.FarmingCropReport,
        account.PrintSupplierCertificateReport,
        module='farming', type_='report')
