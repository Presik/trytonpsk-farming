# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction


class Configuration(ModelSQL, ModelView):
    'Farming Production Configuration'
    __name__ = 'farming.configuration'
    farming_production_sequence = fields.Many2One('ir.sequence',
        'Farming Production Sequence', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    farm_code = fields.Char('Farm Code', required=True)
    charge_form_sequence = fields.Many2One('ir.sequence',
        'Charge Form Sequence', required=True)
    phyto_export_sequence = fields.Many2One('ir.sequence',
        'Phyto Export Sequence', required=True)
    ica_register = fields.Char('ICA Register', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def get_config(cls):
        res = cls.search([
            ('company', '=', Transaction().context.get('company'))
        ])
        return res[0]
