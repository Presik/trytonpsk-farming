# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval


class FarmingSeed(ModelSQL, ModelView):
    'Farming Seed'
    __name__ = 'farming.seed'
    _rec_name = 'number'
    number = fields.Char('Number', required=True)
    kind = fields.Selection([
            ('father', 'Father'),
            ('mother', 'Mother'),
            ('child', 'Child'),
        ], 'Kind', required=True)
    father = fields.Many2One('farming.seed', 'Father',
        states= {
            'required': Eval('kind') == 'child',
        })
    mother = fields.Many2One('farming.seed', 'Mother',
        states= {
            'required': Eval('kind') == 'child',
        })
    target = fields.Char('Target')
    notes = fields.Text('Notes')
