# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval

STATES = {
    'readonly': Eval('state') == 'done'
}


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    boxes = fields.Float('Boxes', digits=(6, 2))
    packing_qty = fields.Float('Pieces', digits=(6, 2))
    quantity = fields.Float('Quantity', digits=(6, 2))
    unit_qty = fields.Float('Stems', digits=(6, 2))

    @fields.depends('unit_qty', 'quantity', 'packing_qty', 'boxes', 'lines')
    def on_change_lines(self):
        if hasattr(self, 'type') and self.type == 'out':
            for name in ('unit_qty', 'quantity', 'packing_qty'):
                values = []
                for line in self.lines:
                    if line.origin and hasattr(line.origin, name):
                        values.append(getattr(line.origin, name) or 0)
                res = round(sum(values), 2)
                setattr(self, name, res)


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'
    packing_qty = fields.Function(fields.Float('Packing Qty', digits=(6, 2)),
        'get_packing_value')
    boxes = fields.Function(fields.Float('Boxes', digits=(6, 2)),
        'get_packing_value')
    stems = fields.Function(fields.Float('Stems', digits=(6, 2)),
        'get_packing_value')

    def get_packing_value(self, name=None):
        if self.origin and hasattr(self.origin, name):
            value = getattr(self.origin, name)
            if value:
                return round(value, 2)
            return 0
