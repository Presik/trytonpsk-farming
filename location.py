# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pyson import Eval, Bool
from trytond.model import ModelView, ModelSQL, fields


class FarmingLocationLotBed(ModelSQL, ModelView):
    'Farming Location Lot Bed'
    __name__ = 'farming.location.lot.bed'
    _rec_name = 'number'
    number = fields.Char('Number', required=True)
    quantity = fields.Integer('Quantity')
    lot = fields.Many2One('farming.location.lot', 'Lot', ondelete='CASCADE', required=True)
    product = fields.Function(fields.Char('product'), 'get_product')

    def get_product(self, name):
        if self.lot.product:
            return self.lot.product.name


class FarmingLocationLot(ModelSQL, ModelView):
    'Farming Location Lot'
    __name__ = 'farming.location.lot'
    _rec_name = 'number'
    number = fields.Char('Number', required=True)
    location = fields.Many2One('farming.location', 'Location')
    product = fields.Many2One('product.product', 'Product')
    quantity = fields.Integer('Quantity', states={
        'readonly': Bool(Eval('beds'))}, depends=['beds'])
    beds = fields.One2Many('farming.location.lot.bed', 'lot', 'Beds')

    @fields.depends('beds')
    def on_change_with_quantity(self, name=None):
        quantity = 0
        if self.beds:
            for bed in self.beds:
                if bed.quantity:
                    quantity += bed.quantity
        return quantity

    def get_rec_name(self, name=None):
        if self.location and self.location.code:
            return self.number + '\u2014' + self.location.code
        else:
            return self.number


class FarmingLocation(ModelSQL, ModelView):
    "Farming Location"
    __name__ = "farming.location"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', states={'required': True})
    address = fields.Char('Address')
    warehouse = fields.Many2One('stock.location', 'Warehouse', required=True,
            domain=[('type', '=', 'warehouse')])
    lots = fields.One2Many('farming.location.lot', 'location', 'Lots')

    @classmethod
    def __setup__(cls):
        super(FarmingLocation, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))
