
from trytond.pool import PoolMeta
from trytond.model import fields, ModelSQL, ModelView


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    carrier = fields.Many2One('party.party', 'Carrier')
    freight_forwader = fields.Many2One('party.party', 'Freight Forwader')
    export_route = fields.Char('Export Route', select=True)
    customs_globals = fields.One2Many('party.customs_global', 'party',
        'Custom Globals')
    # supplier billing information
    invoice_authorization = fields.Many2One('account.invoice.authorization',
        'Invoice Authorization', domain=[('state', '=', 'active'), ('type', '=', 'in')])


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'
    ica_register = fields.Char('ICA Register')
    farm_code = fields.Char('Farm Code')


class CustomsGlobal(ModelSQL, ModelView):
    'Party Customs Global'
    __name__ = 'party.customs_global'
    _rec_name = 'global_custom'
    party = fields.Many2One('party.party', 'Party', ondelete='CASCADE',
        required=True)
    export_target_city = fields.Many2One('party.city_code', 'Target City',
        required=True, select=True)
    global_custom = fields.Char('Global Custom')
