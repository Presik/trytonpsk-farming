
-- DROP TABLE farming_production_crop;
-- DROP TABLE farming_production_work;
-- DROP TABLE farming_production;
--
--
-- ALTER TABLE farming_crop_lot DROP COLUMN number;
-- ALTER TABLE farming_crop_lot_bed DROP COLUMN code;
-- ALTER TABLE farming_crop_stage DROP COLUMN activity_time;


-- DROP TABLE farming_variety_stage CASCADE;

ALTER TABLE farming_variety_cycle RENAME TO farming_variety_stage;
ALTER TABLE farming_crop_activity RENAME TO farming_crop_stage_activity;
ALTER TABLE farming_crop_activity_supply RENAME TO farming_crop_stage_activity_supply;
