# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta


class ProductStyle(ModelSQL, ModelView):
    "Product Color"
    __name__ = "product.style"
    name = fields.Char('Name')


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    farming = fields.Boolean('Farming')
    cost_type = fields.Selection([
            ('', ''),
            ('materials', 'Materials'),
            ('workforce', 'Workforce'),
            ('indirect', 'Indirect'),
        ], 'Cost Type')
    hts = fields.Char('HTS')
    harmonized_tariff_schedule = fields.Many2One('farming.harmonized_tariff_schedule',
                                                 'Harmonized Tariff Schedule')
    # exportation info


class HarmonizedTariffSchedule(ModelSQL, ModelView):
    """HarmonizedTariffSchedule"""
    __name__ = 'farming.harmonized_tariff_schedule'
    name = fields.Char('Name', states={'required': True})
    code = fields.Char('Code', states={'required': True})
    uom = fields.Many2One('product.uom', 'uom',
                          states={'required': True})
