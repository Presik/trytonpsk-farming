# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from itertools import chain
from sql import Table

from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import (
    Wizard, StateReport, StateView, Button, StateTransition)
from trytond.report import Report

STATES = {
    'readonly': Eval('state') != 'draft',
}


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'
    ica_certicate = fields.Many2One('farming.quality.ica', 'Certificado ICA',
        states={
            'readonly': Eval('state').in_(['confirmed', 'processed']),
        })
    delivery_time = fields.Time('Delivery Time')
    delivery_time_fmt = fields.Function(fields.Time('Delivery Time'),
        'get_delivery_time_fmt')
    unit_qty = fields.Function(fields.Integer('Unit Qty'), 'get_unit_qty')

    @fields.depends('ica_certicate')
    def on_change_ica_certicate(self):
        if self.ica_certicate:
            party = self.ica_certicate.party
            self.party = party.id
            self.invoice_address = party.addresses[0].id
            if party.currency:
                self.currency = party.currency.id
            if party.supplier_payment_term:
                self.payment_term = party.supplier_payment_term.id

    def get_delivery_time_fmt(self, name=None):
        if self.delivery_time:
            return str(self.delivery_time)

    def get_unit_qty(self, name=None):
        res = []
        for line in self.lines:
            res.append(line.quantity)
        return int(sum(res))

    def create_shipment(self, shipment_type):
        super(Purchase, self).create_shipment(shipment_type)
        for ship in self.shipments:
            ship.effective_date = self.delivery_date
            ship.save()


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'
    STATES = {
        'invisible': ~Eval('farming'),
    }
    farming = fields.Boolean('Farming')
    capuchon = fields.Selection([
        ('', ''),
        ('si', 'Si'),
        ('no', 'No'),
        ], 'Capuchon', states=STATES)
    patin = fields.Selection([
        ('', ''),
        ('si', 'Si'),
        ('no', 'No'),
        ], 'Patin', states=STATES)
    farming = fields.Boolean('Farming')
    longitud = fields.Integer('Longitud', states=STATES)
    quality_analysis = fields.One2Many('farming.quality.analysis', 'origin',
        'Quality Analysis', states=STATES)
    original_qty = fields.Float('Qty Original', states=STATES)
    qty_checked = fields.Float('Qty Checked', states=STATES)
    returned_qty = fields.Float('Returned Qty', states={'readonly': True})

    @fields.depends('farming', 'original_qty', 'quantity')
    def on_change_farming(self):
        if self.farming and self.quantity:
            self.original_qty = self.quantity

    @fields.depends('quality_analysis', 'original_qty', 'returned_qty')
    def on_change_quality_analysis(self):
        res = []
        for qa in self.quality_analysis:
            if qa.returned_qty:
                res.append(qa.returned_qty)
        self.returned_qty = sum(res)
        self.quantity = self.original_qty - sum(res)

    @fields.depends('original_qty', 'quantity')
    def on_change_original_qty(self):
        if self.original_qty:
            self.quantity = self.original_qty

    def _get_invoice_line_quantity(self):
        return self.quantity


class PurchaseFarmingStart(ModelView):
    'Purchase Farming Report Start'
    __name__ = 'farming.purchase.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class PurchaseFarming(Wizard):
    'Purchase Analytic Report'
    __name__ = 'farming.purchase'
    start = StateView(
        'farming.purchase.start',
        'farming.farming_purchase_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('farming.purchase.report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PurchaseFarmingReport(Report):
    __name__ = 'farming.purchase.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Purchase = pool.get('purchase.purchase')
        purchases = Purchase.search([
            ('company', '=', data['company']),
            ('lines.farming', '=', True),
            ('lines.quantity', '>=', 0),
            ('ica_certicate', '!=', None),
            ('delivery_date', '>=', data['start_date']),
            ('delivery_date', '<=', data['end_date']),
            ], order=[('delivery_date', 'ASC')])

        report_context['records'] = purchases
        report_context['company'] = Company(data['company'])
        return report_context


class GroupingPurchasesStart(ModelView):
    'Grouping Purchases Start'
    __name__ = 'purchase.grouping_purchases.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    supplier = fields.Many2One('party.party', 'Supplier', required=True)
    currency = fields.Many2One('currency.currency', 'Currency', required=True)
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)
    ica_certicate = fields.Many2One('farming.quality.ica', 'Certificado ICA',
        domain=[
            ('party', '=', Eval('supplier'))
        ])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class GroupingPurchases(Wizard):
    'Grouping Purchases'
    __name__ = 'purchase.grouping_purchases'
    start = StateView(
        'purchase.grouping_purchases.start',
        'farming.grouping_purchases_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def _create_invoice(self, lines, purchase):
        invoice_lines = []
        for line in lines:
            line_ = line.get_invoice_line()
            invoice_lines.append(line_)

        invoice_lines = list(chain(*invoice_lines))
        if not invoice_lines:
            return

        invoice = purchase._get_invoice_purchase()
        if getattr(invoice, 'lines', None):
            invoice_lines = list(invoice.lines) + invoice_lines
        invoice.lines = invoice_lines
        invoice.save()

        invoice.update_taxes()
        purchase.copy_resources_to(invoice)
        return invoice

    def transition_accept(self):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        dom = [
            ('state', 'in', ['confirmed', 'processing', 'done']),
            ('invoice_state', '=', 'none'),
            ('party', '=', self.start.supplier.id),
            ('currency', '=', self.start.currency.id),
            ('delivery_date', '>=', self.start.start_date),
            ('delivery_date', '<=', self.start.end_date),
        ]
        if self.start.ica_certicate:
            dom.append(
                ('ica_certicate', '=', self.start.ica_certicate.id)
            )

        purchases = Purchase.search(dom, order=[('delivery_date', 'ASC')])
        if not purchases:
            return 'end'

        lines_to_bill = []
        for purch in purchases:
            if purch.invoices:
                continue
            for line in purch.lines:
                lines_to_bill.append(line)

        if lines_to_bill:
            self._create_invoice(lines_to_bill, purchases[0])
        return 'end'


class UpdatePurchaseLineStart(ModelView):
    'Update Purchase Line Start'
    __name__ = 'farming.purchase_update_line.start'
    unit_price = fields.Numeric('Unit Price.', digits=(6, 2))


class UpdatePurchaseLine(Wizard):
    'Update Purchase Line'
    __name__ = 'farming.purchase_update_line'
    start = StateView(
        'farming.purchase_update_line.start',
        'farming.purchase_update_line_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        PurchaseLine = Pool().get('purchase.line')
        record_id = Transaction().context['active_id']
        line, = PurchaseLine.browse([record_id])
        print(line.purchase.state, line.purchase.id)
        if line.purchase.invoices or not self.start.unit_price:
            return 'end'

        elif line.purchase.state == 'done' and not line.purchase.invoices:
            Purchase = Pool().get('purchase.purchase')
            _purchase = Table('purchase_purchase')
            purchase, = Purchase.browse([line.purchase])
            cursor = Transaction().connection.cursor()
            cursor.execute(*_purchase.update(
                columns=[_purchase.state],
                values=['quotation'],
                where=_purchase.id == purchase.id)
            )

        _line = Table('purchase_line')
        cursor = Transaction().connection.cursor()
        cursor.execute(*_line.update(
            columns=[_line.unit_price],
            values=[self.start.unit_price],
            where=_line.id == record_id)
        )
        line.purchase.save()
        return 'end'
