# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Work(metaclass=PoolMeta):
    __name__ = 'production.work'
    employee = fields.Many2One('company.employee', 'Employee', required=False)
    start_dt = fields.DateTime('Start Datetime')
    end_dt = fields.DateTime('End Datetime')
    total_time = fields.Function(fields.Float('Total Time', digits=(16, 2),
        help="Time in minutes"), 'get_total_time')

    def get_total_time(self, name=None):
        if self.start_dt and self.end_dt:
            return round((self.end_dt - self.start_dt).seconds / 60, 2)
